# Desafio Stoom - Back End

Desafio com objetivo de construir uma aplicação Rest em Java com CRUD

Contatos:
--
  -  (31)99868-1292
  - anderson_versteeg@hotmail.com
    

# Requisitos para subir e testar a aplicação com Postman

  - Ter Maven instalado
  - Ter Postman(Extensão do Chrome ou versão Desktop)
  
Links importantes: 
    - Site do [Maven][maven] para instalação do mesmo caso não possua
    - Site do [git][git] para instalação do mesmo caso não possua
	- Site do [Docker][docker] para instalação do mesmo caso não possua
    - Extensão do [Postman][postman] para Google Chrome	
    - A aplicação sobe na porta padrão [localhost:8080][local]
    - O banco de dados em memória é acessivel pela seguinte url [localhost:8080/h2][h2]
    - A documentação em formato Json da aplicação se encontra em [localhost:8080/v2/api-docs][apiDoc]
    - A documentação em UI se econtra em [localhost:8080/swagger-ui.html][swaggerUi]
    

### Subindo a aplicação

Clonando o Respositório do projeto, escolha um diretório e utilize o seguinte comando

```sh
$ git clone https://gitlab.com/aversteeg/desafio-stoom.git
```
Instalando dependências do maven

```sh
$ cd desafio-stoom
$ mvn install
```

Rodando todos os Testes Unitários com Maven

```sh
$ mvn test
```

Subindo Aplicação

```sh
$ java -jar target/stoom-0.0.1-SNAPSHOT.jar
```

Subindo Aplicação via Docker:
### Notes
- Para subir a aplicação via Docker é necessário ter o Docker instalado previamente em sua máquina.

```sh
$ docker build -t stoom/stoom-app-challange .
```

- Após gerar o Container basta rodar o mesmo com o seguinte comando:

```sh
$  docker run -p 8080:8080 stoom/stoom-app-challange
```

- Caso encontre algum problema é possível verificar através do Docker se a imagem realmente foi gerada corretamente para uso através dos passos executados: 

![dockerImage](/images/dockerImage.JPG)

### Notes
- O processo de subir a aplicação e rodar os testes unitários podem ser feitos de dentro de uma IDE de sua preferência como Eclipse ou IntelliJ por exemplo, para subir a aplicação basta rodar a classe StoomApplication.java como uma Java Application, os testes basta selecionar o teste desejado e rodar como um JUnit Test.

- Ao acessar a url: localhost:8080/h2 para utilziação do banco de dados as configurações são as seguintes:

![h2Config](/images/h2.JPG)

- Para enviar requisições pelo Postman basta abrir o mesmo, importar a coleção de nome desafio_stoom.postman_collection que se encontra na raiz do repositório, nesta coleção se encontram alguns testes básicos do CRUD com as principais
ações que são executadas pela aplicação.

![postmanExample](/images/postman.JPG)

- Caso deseje saber todos endpoints e requisições possíveis basta acessar a UI do [Swagger][swaggerUi] onde os mesmos estão documentados:

![swagger](/images/swaggerUi.JPG)




   [maven]: <https://maven.apache.org/install.html>
   [git]: <https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git>
   [docker]: <https://www.docker.com/get-started>
   [postman]: <https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop>
   [local]: <localhost:8080>
   [h2]: <localhost:8080/h2>
   [apiDoc]: <localhost:8080/v2/api-docs>
   [swaggerUi]: <localhost:8080/swagger-ui.html>